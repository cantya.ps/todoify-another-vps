package todoify.taskservice.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;

@Entity
public class TaskModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "userId")
    @NotEmpty
    private String userId;

    @Column(name = "title")
    @NotEmpty
    private String title;

    @Column(name = "date_time")
    @NotNull
    @Temporal(TemporalType.DATE)
    private java.util.Date date;

    @Column(name = "description")
    @NotEmpty
    private String description;

    public TaskModel() {
    }

    public TaskModel(String userId, String title, Date date, String description) {
        this.userId = userId;
        this.title = title;
        this.date = date;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() { return userId;}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "TaskModel{" +
                "userId='" + userId + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
