package todoify.taskservice.repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import todoify.taskservice.entity.TaskModel;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends CrudRepository<TaskModel,Long> {
    List<TaskModel> findAllByTitle(String title);

    @Transactional
    List<TaskModel> deleteByTitle(String title);

    List<TaskModel> findByTitleAndDateAndUserId(String title, Date date, String user_id);
}
