package todoify.taskservice.service;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Service;
import todoify.taskservice.entity.TaskModel;
import todoify.taskservice.repository.TaskRepository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

@Service
public class RemoveService {
    private final TaskRepository taskRepository;
    public RemoveService(TaskRepository taskRepository) { this.taskRepository = taskRepository; }

    public Message handle(String argument,String userId) throws ParseException {
        if(argument.equals("none")){
            return new TextMessage(
                    "We cant process your form! please use '/removeTemplate' for a valid remove form!"
            );
        }else{
            String[] splittedArgument = argument.split("\n");
            for (int i= 0; i < splittedArgument.length; i++){
                splittedArgument[i] = splittedArgument[i].split(":",2)[1];
            }

            String value = splittedArgument[0].trim();
            System.out.println(value.trim());

            Iterable<TaskModel> dataFromDatabase = taskRepository.findAll();

            System.out.println("Trying to delete data");
            System.out.println("---------------------------------------");
            List<TaskModel> dataTobeDelete = new ArrayList<>();
            for(TaskModel taskModel : dataFromDatabase) {
                //System.out.println("Found : " + taskModel.toString());
                if(taskModel.getTitle().trim().equals(value)){
                    dataTobeDelete.add(taskModel);
                }
            }

            System.out.println("------------------------------------------------ DELETE");

            for(TaskModel data : dataTobeDelete) {
                System.out.println("Found data to be delete : "+ data.toString());
            }

            if(dataTobeDelete.size() > 0) {
                taskRepository.deleteAll(dataTobeDelete);
                return new TextMessage("Your task been successfully removed!");
            } else {
                return new TextMessage("Tidak menemukan data dengan title " + value);
            }
        }
    }
}
