package todoify.botservice.clients;


import java.util.ArrayList;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient("taskService")
public interface TaskClient {
    @GetMapping("/handler")
    void handleQuery(@RequestParam("query") String query, @RequestParam("argument") String arguments, @RequestParam("replyToken") String replyToken,@RequestParam("userId") String userId);

}
