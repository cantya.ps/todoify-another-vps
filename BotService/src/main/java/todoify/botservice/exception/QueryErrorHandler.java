package todoify.botservice.exception;

public class QueryErrorHandler extends RuntimeException {
    public QueryErrorHandler(String errorMessage) {
        super(errorMessage);
    }
}
